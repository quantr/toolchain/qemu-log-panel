package hk.quantr.qemulogpanel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.commons.io.IOUtils;

import com.thoughtworks.xstream.XStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Setting {

	private static Setting setting = null;
	public static File file = new File("setting.xml");
	public int x;
	public int y;
	public int width;
	public int height;
	public int dividerLocation;

	public String compareDialog_from;
	public String compareDialog_to;
	public String compareDialog_limit;
	public String compareDialog_qemu;
	public String compareDialog_quantr;
	public String compareDialog_excludeMemAddr;

	public String listErrorDialog_from;
	public String listErrorDialog_to;
	public String listErrorDialog_limit;
	public ArrayList<String> listErrorTable = new ArrayList();

	public int pageNo;
	public int rowNo;

	public Setting() {
		width = 800;
		height = 600;
	}

	public static Setting getInstance() {
		if (setting == null) {
			setting = load();
		}
		return setting;
	}

	public void save() {
		XStream xstream = new XStream();
		xstream.alias("Setting", Setting.class);
		String xml = xstream.toXML(this);
		try {
			IOUtils.write(xml, new FileOutputStream(file), "utf8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Setting load() {
		try {
			XStream xstream = new XStream();
			xstream.allowTypes(new Class[]{Setting.class});
			xstream.alias("Setting", Setting.class);
			Setting s = (Setting) xstream.fromXML(new FileInputStream(file));
			return s;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			file.delete();
			setting = new Setting();
			setting.save();
			return setting;
		}
	}

}
