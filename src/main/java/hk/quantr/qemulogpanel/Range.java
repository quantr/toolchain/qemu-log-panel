/*
 * Copyright 2024 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Range {

	public int start, end;

	public Range(int st, int en) {
		start = st;
		end = en;
	}

	@Override
	public String toString() {
		return "Range (" + start + ", " + end + ") " + (end - start) + "\n";
	}

	public static Range[] getRanges(int start, int end, int noOfRange) {
		float len = (end - start) / noOfRange;
		Range r[] = new Range[noOfRange];
		int lastStart = start;
		for (int x = 0; x < r.length; x++) {
			r[x] = new Range(lastStart, (int) (lastStart + len));
			lastStart = r[x].end + 1;
		}
		r[r.length - 1].end = end;
		return r;
	}
}
