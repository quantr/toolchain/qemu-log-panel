/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class DiffTableModel extends AbstractTableModel {

	Logger logger = Logger.getLogger(DiffTableModel.class.getName());
	HashMap<String, CellData> data;
	public ArrayList<String> putOnTopRegisters;

	public DiffTableModel() {
	}

	@Override
	public int getRowCount() {
		return data == null ? 0 : data.size();
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		if (column == 0) {
			return "";
		} else if (column == 1) {
			return "Qemu";
		} else {
			return "Quantr";
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		List<Object> registerNames = Arrays.asList((Object[]) data.keySet().toArray());
		Collections.sort(registerNames, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				String s1 = (String) o1;
				String s2 = (String) o2;
				if (putOnTopRegisters != null) {
					if (s1.contains("_")) {
						if (putOnTopRegisters.contains(s1.split("_")[0].toLowerCase()) || putOnTopRegisters.contains(s1.split("_")[1].toLowerCase())) {
							return -1;
						}
					} else {
						if (putOnTopRegisters.contains(s1.toLowerCase())) {
							return -1;
						}
					}
					if (s2.contains("_")) {
						if (putOnTopRegisters.contains(s2.split("_")[0].toLowerCase()) || putOnTopRegisters.contains(s2.split("_")[1].toLowerCase())) {
							return 1;
						}
					} else {
						if (putOnTopRegisters.contains(s2.toLowerCase())) {
							return 1;
						}
					}
				}
				if (s1.equals("PC")) {
					return -1;
				} else if (s2.equals("PC")) {
					return 1;
				} else {
					return s1.compareTo(s2);
				}
			}
		});

		if (columnIndex == 0) {
			return registerNames.get(rowIndex);
		} else if (columnIndex == 1) {
			return data.get(registerNames.get(rowIndex)).qemuValue;
		} else {
			return data.get(registerNames.get(rowIndex)).quantrValue;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
	}
}
