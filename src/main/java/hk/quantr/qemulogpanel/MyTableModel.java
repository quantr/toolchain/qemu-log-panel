/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import hk.quantr.javalib.CommonLib;
import java.io.File;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyTableModel extends AbstractTableModel {

	Logger logger = Logger.getLogger(MyTableModel.class.getName());
	final ArrayList<String> columns = new ArrayList();
	public File database;
	int pageNo;
	int pageSize = 10000;
	public ArrayList<HashMap<String, CellData>> data = new ArrayList();
	public ArrayList<HashMap<String, Object>> allQemudata = new ArrayList();
	public ArrayList<Boolean> matches = new ArrayList();
	public ArrayList<Integer> priv = new ArrayList();
	public JSONArray json;
	public int qemuCount = -1;
	public int quantrCount = -1;
	boolean showCCode;
	public static final BigInteger UNSIGNED_LONG_MASK = BigInteger.ONE.shiftLeft(Long.SIZE).subtract(BigInteger.ONE);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public MyTableModel() {
	}

	public static Connection fileToConnection(File database) throws SQLException {
		System.out.println("connecting " + database.getAbsolutePath());
		Connection qemuConn = DriverManager.getConnection("jdbc:h2:" + database.getAbsolutePath().replace(".mv.db", ""), "sa", "");
		return qemuConn;
	}

	public void loadQemuH2Database(File database) throws SQLException {
		this.database = database;
		loadQemuH2Database(fileToConnection(database));
	}

	public void loadQemuH2Database(Connection qemuConn) {
		synchronized (columns) {
//			if (database.exists()) {
			try {
//					System.out.println("opening " + database.getAbsolutePath().replace(".mv.db", ""));
				Statement qemuStmt = qemuConn.createStatement();
				System.out.println("executing: show columns from `qemu`;");
				ResultSet qemuRS = qemuStmt.executeQuery("show columns from `qemu`;");
				columns.clear();
				while (qemuRS.next()) {
					String columnName = qemuRS.getString("column_name");
					if (!showCCode && columnName.equals("CCODE")) {
						continue;
					}
					if (!columnName.equals("COMPUTER") && !columnName.equals("DATE") && !columnName.equals("GUID") && !columnName.equals("ID") && !columnName.equals("MEM") /*&& !columnName.equals("SEQUENCE")*/) {
						columns.add(columnName);
					}
				}
				Collections.sort(columns, new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						return o1.compareTo(o2);
					}
				});
				columns.remove("PC");
				columns.add(0, "M");
				columns.add(1, "Row");
				columns.add(2, "PC");
				columns.add(3, "Quantr PC");
				columns.add(4, "Line No");
				columns.add(5, "Quantr Code");
				columns.add(6, "File");
				columns.add(7, "Priv");

				qemuStmt = qemuConn.createStatement();
				if (qemuCount == -1) {
					qemuRS = qemuStmt.executeQuery("select count(*) from `qemu`;");
					qemuRS.next();
					qemuCount = qemuRS.getInt(1);
				}
//					System.out.println(sdf.format(new Date()) + " s1");
				qemuRS = qemuStmt.executeQuery("select * from `qemu` where sequence>=" + (pageNo * pageSize) + " order by sequence limit " + pageSize);
				data.clear();
				allQemudata.clear();
				matches.clear();
				priv.clear();

//					Connection quantrConn = null;
				ResultSet quantrRS = null;
				ArrayList<String> quantrRegisters = null;
				boolean hasAtLeastOneRecord;
//						quantrConn = DriverManager.getConnection("jdbc:h2:" + quantrH2.getAbsolutePath().replace(".mv.db", ""), "sa", "");

				Statement quantrStmt = qemuConn.createStatement();
				if (quantrCount == -1) {
					quantrRS = quantrStmt.executeQuery("select count(*) from `quantr`;");
					quantrRS.next();
					quantrCount = quantrRS.getInt(1);
				}

				quantrRegisters = new ArrayList();
				quantrRS = quantrStmt.executeQuery("show columns from `quantr`;");
				while (quantrRS.next()) {
					String columnName = quantrRS.getString("column_name");
					quantrRegisters.add(columnName);
				}

				//quantrRS = quantrStmt.executeQuery("select * from `qemu` where id>=" + (pageNo * pageSize) + " order by id limit " + pageSize);
				String sql = "select * from `quantr` where sequence>=" + (pageNo * pageSize) + " and sequence<" + ((pageNo + 1) * pageSize) + " order by sequence limit " + pageSize;
				System.out.println(sql);
				quantrRS = quantrStmt.executeQuery(sql);
				if (!quantrRS.next()) {
					quantrRS.close();
					quantrRS = null;
				}
				boolean hasQuantr = false;

//					System.out.println(sdf.format(new Date()) + " s2");
				int index = 0;
				while (qemuRS.next()) {
//					if (qemuRS.getLong("sequence") == 10690654l) {
//						System.out.println("fuck");
//					}
					if (quantrRS != null) {
						if (qemuRS.getLong("sequence") == quantrRS.getLong("sequence")) {
							hasQuantr = true;
						} else {
							hasQuantr = false;
						}
					} else {
						hasQuantr = false;
					}
					HashMap<String, CellData> d = new HashMap();
					boolean match = true;
					for (String column : columns) {
						if (column.equals("Line No")) {
							column = "lineNo";
						} else if (!column.equals("M") && !column.equals("Row") && !column.equals("Quantr PC") && !column.equals("Quantr Code") && !column.equals("File") && !column.equals("Priv") /*&& !column.equals("FILENAME")&& !column.equals("CODELINENO")*/) {
							CellData tableData = new CellData();

							tableData.registerName = column;
							if (CommonLib.isNumber(qemuRS.getString(column))) {
								tableData.qemuValue = CommonLib.string2BigIntegerWithNull(qemuRS.getString(column));
								if (tableData.qemuValue != null) {
									tableData.qemuValue = ((BigInteger) tableData.qemuValue).and(UNSIGNED_LONG_MASK);
								}
							} else {
								tableData.qemuValue = qemuRS.getObject(column);
							}

							if (tableData.qemuValue != null && column.equals("CODE")) {
								tableData.qemuValue = ((String) tableData.qemuValue).replaceFirst("^.*: ", "");
							}

							// add quantr db
							int tempX = (pageNo * pageSize) + index;
							if (hasQuantr) {
								if (column.equals("CODE") /*&& quantrRS.getString("Bytes") != null && quantrRS.getString("Line") != null*/) {
									tableData.quantrValue = quantrRS.getString("Bytes").replaceAll("0x", "") + " " + quantrRS.getString("Line");
								} else if (column.equals("FILENAME")) {
									tableData.quantrValue = quantrRS.getString("FILENAME");
								} else {
									String realRegisterName = column.split("_")[0];
									if (quantrRS != null && quantrRegisters.contains(realRegisterName)) {
										tableData.quantrValue = BigInteger.valueOf(quantrRS.getLong(realRegisterName)).and(UNSIGNED_LONG_MASK);
									}
								}
							} else if (json != null) {
								if (tempX >= json.length()) {
									matches.add(Boolean.FALSE);
								} else {
									JSONObject obj = json.getJSONObject(tempX);
									String registerName = column.contains("_") ? column.toLowerCase().split("_")[0] : column.toLowerCase();
									try {
										BigInteger b = obj.getJSONObject("registers").getJSONObject(registerName).getBigInteger("value");
										tableData.quantrValue = b;
									} catch (JSONException ex) {
										ex.printStackTrace();
									}
								}
							}
							// end add quantr db

							// check match
							if (!column.equals("MIP") && !column.equals("MEMOPERATION") && !column.equals("MEMVALUE") && !column.equals("CODELINENO")) { // ignore columns
								Object o1 = tableData.qemuValue;
								Object o2 = tableData.quantrValue;
								if (o1 instanceof BigInteger && o2 instanceof BigInteger) {
									BigInteger b1 = (BigInteger) o1;
									BigInteger b2 = (BigInteger) o2;

									if (!b1.equals(b2)) {
										match = false;
									}
								}
							}
							if (!hasQuantr) {
								match = false;
							}
							// end check match

							d.put(column, tableData);
						}
					}
					if (quantrRS != null && hasQuantr) {
						priv.add(quantrRS.getInt("priv"));
					}
					data.add(d);

					// add all qemu columns
					ResultSetMetaData metadata = qemuRS.getMetaData();
					int columnCount = metadata.getColumnCount();
					HashMap<String, Object> temp = new HashMap();
					for (int i = 1; i <= columnCount; i++) {
						temp.put(metadata.getColumnName(i), qemuRS.getObject(metadata.getColumnName(i)));
					}
					allQemudata.add(temp);
					// end add all qemu columns

					matches.add(match);
					index++;

					if (quantrRS != null) {
						if (qemuRS.getLong("sequence") == quantrRS.getLong("sequence")) {
							if (!quantrRS.next()) {
								quantrRS = null;
							}
						}
					}
				}

				columns.remove("FILENAME");
				columns.remove("CODELINENO");

//					System.out.println(sdf.format(new Date()) + " s3");
//					qemuConn.close();
//					if (quantrH2 != null) {
//						quantrConn.close();
//					}
			} catch (SQLException ex) {
				logger.log(Level.SEVERE, null, ex);
			}
//			}
		}
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		synchronized (columns) {
			return columns.size();
		}
	}

	@Override
	public String getColumnName(int column) {
		synchronized (columns) {
			if (columns.isEmpty()) {
				return null;
			}
			return columns.get(column);
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (data == null || data.isEmpty() || rowIndex >= data.size() || matches == null || matches.isEmpty()) {
			return null;
		}
		String columnName = getColumnName(columnIndex);
//		if (columnIndex == 0) {
//			int x = 12;
//		}
//		if (columnName.equals("PC")){
//			int x=12;
//		}
		if (columnName == null) {
			return null;
		} else if (columnName.equals("M")) {
			return matches.get(rowIndex);
		} else if (columnName.equals("Row")) {
			return (pageNo * pageSize) + rowIndex;
		} else if (columnName.equals("Line No")) {
			return data.get(rowIndex).get("LINENO").qemuValue;
		} else if (columnName.equals("Quantr PC")) {
			int tempX = (pageNo * pageSize) + rowIndex;
			if (json != null && tempX < json.length()) {
				return json.getJSONObject(tempX).getJSONObject("registers").getJSONObject("pc").getBigInteger("value");
			} else if (database != null) {
				int temp = getColumnIndex("PC");
				return ((CellData) getValueAt(rowIndex, temp)).quantrValue;
			} else {
				return null;
			}
		} else if (columnName.equals("Quantr Code")) {
			int tempX = (pageNo * pageSize) + rowIndex;
			if (database != null) {
				int temp = getColumnIndex("CODE");
				return ((CellData) getValueAt(rowIndex, temp)).quantrValue;
			} else {
				return null;
			}
		} else if (columnName.equals("File")) {
			String qemu = data.get(rowIndex).get("FILENAME").qemuValue == null ? "" : (String) data.get(rowIndex).get("FILENAME").qemuValue + ":" + data.get(rowIndex).get("CODELINENO").qemuValue;
			String quantr = (String) data.get(rowIndex).get("FILENAME").quantrValue + ":" + data.get(rowIndex).get("CODELINENO").quantrValue;
			if (qemu.equals(quantr)) {
				return qemu;
			} else {
				return qemu + " / " + quantr;
			}
		} else if (columnName.equals("Priv")) {
			if (rowIndex < priv.size()) {
				return priv.get(rowIndex);
			} else {
				return null;
			}
		} else if (columnName.equals("INTERRUPT")) {
			return data.get(rowIndex).get(columnName).qemuValue;
		} else {
			return data.get(rowIndex).get(columnName);
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
	}

	public int getColumnIndex(String columnName) {
		int x = 0;
		for (String c : columns) {
			if (c.equals(columnName)) {
				return x;
			}
			x++;
		}
		return -1;
	}
}
