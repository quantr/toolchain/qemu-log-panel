/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.helper.VariableWithAddress;
import hk.quantr.javalib.CommonLib;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import me.tongfei.progressbar.ProgressBar;
import org.h2.jdbc.JdbcSQLDataException;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyThread implements Runnable {

	long pc;
	long sequence;
	ArrayList<String> lines;
	static Pattern p = Pattern.compile("\\[(.*)\\]");
	static Pattern r = Pattern.compile(".*addr (0x[0-9a-fA-F]+).* value (0x[0-9a-fA-F]+).* size ([0-9]+)");
	LinkedHashMap<String, String> values = new LinkedHashMap();
	static final HashMap<Long, String> cache = new HashMap();
	static final HashMap<Long, VariableWithAddress> cache2 = new HashMap();
	ProgressBar progressBars[];
	long lineNo;
	ConcurrentHashMap<Long, SourceData> allCode;
	PreparedStatement stmt;
	AtomicLong noOfPending;
	int priv;
	ArrayList<Dwarf> dwarfArrayList;
	final int BATCH_SIZE = 100000;

	/*
	this check sequence is continuous or not
	
	with temp as
      (select sequence,
              lead(sequence) over (order by sequence) lm_sequence
       from `qemu`
      )
    select sequence
    From temp
    where lm_sequence - sequence > 1
    order by sequence;

	SELECT * FROM `qemu` where sequence>=10397157
	 */
	public MyThread(ArrayList<Dwarf> dwarfArrayList, long pc, long sequence, ArrayList<String> lines, ProgressBar[] progressBars, long lineNo, ConcurrentHashMap<Long, SourceData> allCode, int priv, PreparedStatement stmt, AtomicLong noOfPending) {
		this.dwarfArrayList = dwarfArrayList;
		this.pc = pc;
		this.sequence = sequence;
		this.lines = lines;
		this.progressBars = progressBars;
		this.lineNo = lineNo;
		this.allCode = allCode;
		this.stmt = stmt;
		this.noOfPending = noOfPending;
		this.priv = priv;
	}

	@Override
	public void run() {
		try {
//			String threadName = Thread.currentThread().getName();
//			System.out.println("threadName=" + threadName);
//			threadName = threadName.split("-")[1];
//			int threadID = Integer.parseInt(threadName) - 1;
			int threadID = Integer.parseInt(Thread.currentThread().getName().replaceAll(".*-", "")) - 1;
//			System.out.println("threadID=" + threadID);
//			System.out.println("threadID=" + threadID);
//		System.out.println(threadID + " = " + Thread.currentThread().getName() + " = " + sequence);

//			long pc = -1;
			String code = null;
			boolean mem = false;
			String memOperation = null;
			long memAddr = 0;
			long memValue = 0;
			short memSize = 0;
			boolean memRead = false;

			boolean memHiJack = false;
			String memHiJackOperation = null;
			long memHiJackAddr = 0;
			long memHiJackValue = 0;
			short memHiJackSize = 0;
			boolean memHiJackRead = false;

			boolean interrupt = false;
			long cause = -1;
			String desc = null;
			boolean irqRequest = false;
			int irqRequestNo = -1;
			int irqRequestLevel = -1;
//			boolean skip = false;
//			long skipAddress = 0;

//			String dump = "";
			for (String line : lines) {
//				dump += line + "\n";
				if (line.equals("") || line.startsWith("IN") || line.contains("----") || line.contains("V      =   0") || line.contains("Trace") || line.contains("riscv_cpu_tlb_fill") || line.contains("cpu_io_recompile")) {
				} else if (line.startsWith("> mem")) {
					mem = true;
					String temp[] = line.split(",");
					memOperation = temp[0];
					memAddr = CommonLib.string2long(temp[1]);
					memValue = CommonLib.string2long(temp[3]);
					memSize = 0;//CommonLib.string2short(m.group(3));
					if (line.contains("load")) {
						memRead = true;
					}
				} else if (line.startsWith("sifive_plic_irq_request")) {
					irqRequest = true;
					irqRequestNo = Integer.parseInt(line.split(",")[1].split("=")[1]);
					irqRequestLevel = Integer.parseInt(line.split(",")[2].split("=")[1]);
				} else if (line.startsWith("Stopped execution of TB chain")) {
//					skip = true;
//					Matcher m = p.matcher(line);
//					m.find();
//					skipAddress = CommonLib.string2long("0x" + m.group(1));
				} else if (line.startsWith("riscv_cpu_do_interrupt")) {
					interrupt = true;
//					System.out.println(line.replaceFirst("^.*?:", ""));
					String temp[] = line.replaceFirst("^.*?:", "").split(",");
//					System.out.println(temp.length);
					cause = CommonLib.string2long(temp[2].split(":")[1].trim());
					desc = temp[5].split("=")[1].trim();
//					System.out.println("interrupt");
				} else if (line.startsWith("0x")) {
					code = line;
//				} else if (line.startsWith("riscv_cpu_tlb_fill")) {
//					if (line.startsWith("riscv_cpu_tlb_fill ad") && (line.contains("rw 0") || line.contains("rw 1"))) {
//						mem = line.replaceFirst("riscv_cpu_tlb_fill ad", "");
//						mem = mem.replaceAll("mmu_idx.*", "");
//						mem = mem.trim();
//					}
				} else if (line.startsWith("memory_region")) {
					memHiJack = true;
					memHiJackOperation = line;
					Matcher m = r.matcher(line);
					m.find();
					memHiJackAddr = CommonLib.string2long(m.group(1));
					memHiJackValue = CommonLib.string2BigInteger(m.group(2)).longValue();
					memHiJackSize = CommonLib.string2short(m.group(3));
					if (line.contains("read")) {
						memHiJackRead = true;
					}
				} else {
					String words[] = line.trim().split(" +");
					for (int x = 0; x < words.length - 1; x += 2) {
						words[x] = words[x].replaceAll("/", "_");
//						if (x + 1 > words.length - 1) {
//							System.out.println("fuck : " + line);
//							System.exit(1);
//						}
						String value = words[x + 1].startsWith("0x") ? words[x + 1] : "0x" + words[x + 1];
						if (!words[x].contains(":")) {
							values.put(words[x], value);
						}
//						if (words[x].trim().equals("pc")) {
//							pc = CommonLib.string2long(value);
//						}
					}
				}
			}
//			if (skip && pc == skipAddress) {
//				return;
//			}
			synchronized (cache) {
				if (code == null) {
					code = cache.get(pc);
				} else {
					cache.put(pc, code);
				}
			}
//			System.out.printf("%x=%s\n", pc, code);
			SourceData cCode = allCode.get(pc);

			synchronized (stmt) {
				int x = 1;
				try {
					VariableWithAddress v;
					synchronized (cache2) {
						v = cache2.get(pc);
						if (v == null) {
							v = QuantrDwarf.getFilePathAndLineNo(dwarfArrayList, BigInteger.valueOf(pc));
							cache2.put(pc, v);
						}
					}
					stmt.setLong(x++, sequence);
					stmt.setBoolean(x++, interrupt);
					stmt.setLong(x++, cause);
					stmt.setString(x++, desc);
					for (Map.Entry me : values.entrySet()) {
//					System.out.println(me.getKey() + "\t= " + me.getValue());
//						System.out.println(">" + me.getKey() + "=" + me.getValue());
						stmt.setLong(x++, CommonLib.string2long((String) me.getValue()));
					}
					stmt.setLong(x++, lineNo);
					stmt.setString(x++, (String) code);
					stmt.setBoolean(x++, mem);
					stmt.setString(x++, (String) memOperation);
					stmt.setBoolean(x++, memRead);
					stmt.setLong(x++, memAddr);
					stmt.setLong(x++, memValue);
					stmt.setInt(x++, memSize);
					stmt.setBoolean(x++, memHiJack);
					stmt.setString(x++, (String) memHiJackOperation);
					stmt.setBoolean(x++, memHiJackRead);
					stmt.setLong(x++, memHiJackAddr);
					stmt.setLong(x++, memHiJackValue);
					stmt.setInt(x++, memHiJackSize);
					stmt.setString(x++, v == null ? null : v.file.getName());
					stmt.setInt(x++, v == null ? -1 : v.line_num);
					stmt.setString(x++, cCode == null ? null : cCode.code);
					stmt.setInt(x++, priv);
					stmt.setBoolean(x++, irqRequest);
					stmt.setInt(x++, irqRequestNo);
					stmt.setInt(x++, irqRequestLevel);
//					System.out.println("-".repeat(100));
//					System.out.println(values.size());
				} catch (JdbcSQLDataException ex) {
					ex.printStackTrace();
					System.out.println("-".repeat(100));
					System.out.println("sequence=" + sequence);
					System.out.println("cause=" + cause);
					System.out.println("desc=" + desc);
					for (Map.Entry me : values.entrySet()) {
						System.out.println(me.getKey() + "\t= " + me.getValue());
					}
					System.out.println("code=" + code);
					System.out.println("priv=" + priv);
					System.out.println("mem=" + mem);
					System.out.println("memOperation=" + memOperation);
					System.out.println("memRead=" + memRead);
					System.out.println("memAddr=" + memAddr);
					System.out.println("memValue=" + memValue);
					System.out.println("memSize=" + memSize);
					System.out.println("cCode=" + (cCode == null ? null : cCode.code));
					System.out.println("irqRequest=" + irqRequest);
					System.out.println("irqRequestNo=" + irqRequestNo);
					System.out.println("irqRequestLevel=" + irqRequestLevel);

//					System.out.println("wrong " + values.size());
//					System.out.println(dump);
					System.exit(111);
				}

				stmt.addBatch();
//				stmt.execute();

				if (sequence % BATCH_SIZE == 0) {
					int temp[] = stmt.executeBatch();
					for (int xx : temp) {
						if (xx != 1) {
							System.out.println("ERROR, rows failed to insert to db");
							System.exit(123);
						}
					}
//				conn.commit();
				}
			}

			progressBars[threadID].stepTo(lineNo);
//			System.out.println("<<" + noOfPending);
			noOfPending.addAndGet(-1);
//			System.out.println(">>" + noOfPending);
		} catch (SQLException ex) {
			Logger.getLogger(MyThread.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(1);
		}
	}

}
