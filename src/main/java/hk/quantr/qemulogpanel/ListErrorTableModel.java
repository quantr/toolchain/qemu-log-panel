/*
 * Copyright 2024 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ListErrorTableModel extends AbstractTableModel {

	public ArrayList<String> data = new ArrayList();

	public ListErrorTableModel() {
	}

	@Override
	public int getRowCount() {
		List<String> newList = data.stream().distinct().collect(Collectors.toList());
		return newList == null ? 0 : newList.size();
	}

	@Override
	public int getColumnCount() {
		return 1;
	}

	@Override
	public String getColumnName(int column) {
		if (column == 0) {
			return "Sequence";
		} else if (column == 1) {
			return "Row No";
		} else if (column == 2) {
			return "PC";
		} else {
			return "";
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		List<String> newList = data.stream().distinct().collect(Collectors.toList());
		Collections.sort(newList, (String o1, String o2) -> {
			Long l1 = Long.valueOf(o1.split(" - ")[0]);
			Long l2 = Long.valueOf(o2.split(" - ")[0]);
			return l1.compareTo(l2);
		});
		if (columnIndex == 0) {
			return newList.get(rowIndex);
		} else {
			return "";
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Long.class;
	}
}
