/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import hk.quantr.javalib.CommonLib;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CompareTableModel extends AbstractTableModel {

	Logger logger = Logger.getLogger(CompareTableModel.class.getName());
	public String qemu;
	public String quantr;
	public ArrayList<String[]> data = new ArrayList();

	public CompareTableModel() {
	}

	@Override
	public int getRowCount() {
		return data == null ? 0 : data.size();
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public String getColumnName(int column) {
		if (column == 0) {
			return "Sequence";
		} else if (column == 1) {
			return "PC";
		} else if (column == 2) {
			return "Code";
		} else if (column == 3) {
			return "MemAddr";
		} else if (column == 4) {
			return "Qemu";
		} else {
			return "Quantr";
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (columnIndex == 0 || columnIndex == 2) {
//			System.out.println("1");
			return data.get(rowIndex)[columnIndex];
//			System.out.println("1");
		} else {
//			return "AA";
			BigInteger b = CommonLib.string2BigIntegerWithNull(data.get(rowIndex)[columnIndex]);
			if (b != null) {
				b = b.and(MyTableModel.UNSIGNED_LONG_MASK);
			} else {
				return null;
			}
			String result = b.toString(16);
//		System.out.println(data.get(rowIndex)[columnIndex]+", "+result);
			result = new StringBuilder(result).reverse().toString().replaceAll("(.{4})", "$0 ").trim();
			return new StringBuilder(result).reverse().toString();
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}
}
