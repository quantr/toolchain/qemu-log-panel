package hk.quantr.qemulogpanel;

import java.io.File;

/**
 *
 * @author peter
 */
public class SourceData {

    public long address;
    public int lineNo;
    public File file;
    public String code;

    public SourceData(long address, int lineNo, File file, String code) {
	   this.address = address;
	   this.lineNo = lineNo;
	   this.file = file;
	   this.code = code;
    }
 
}
