/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import java.awt.Color;
import java.awt.Component;
import java.math.BigInteger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;

import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyTableRenderer extends JLabel implements TableCellRenderer {

	Color selectedBackground = Color.decode("#eee4e1");
	Color matchColor = Color.decode("#caf0f8");
	Color matchColorSelected = Color.decode("#8ecae6");
	Color notMatchColor = Color.decode("#ffc8dd");
	Color notMatchColorSelected = Color.decode("#e5989b");

	boolean dimUnchange;
	ImageIcon tick = new ImageIcon(getClass().getClassLoader().getResource("image/tick.png"));
	ImageIcon cross = new ImageIcon(getClass().getClassLoader().getResource("image/cross.png"));

	public MyTableRenderer() {
		super();
		setOpaque(true);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		setHorizontalTextPosition(LEFT);
		setIcon(null);
		if (isSelected) {
			setBackground(selectedBackground);
		} else {
			setBackground(UIManager.getColor("Table.background"));
		}
		if ((row + 1) % 10 == 0) {
			setBorder(new MatteBorder(0, 1, 1, 0, Color.gray));
		} else {
			setBorder(new MatteBorder(0, 1, 0, 0, Color.gray));
		}
		if (dimUnchange && row > 0 && value instanceof CellData) {
			CellData obj = (CellData) table.getValueAt(row - 1, column);
			if (obj != null && obj.qemuValue != null && obj.qemuValue.equals(((CellData) value).qemuValue)) {
				setForeground(Color.lightGray);
			} else {
				setForeground(Color.black);
			}
		} else {
			setForeground(Color.black);
		}
//		if (table.getColumnName(column).equals("PC")){
//			int fuck=1;
//		}
		if (value != null) {
			if (value instanceof String) {
				String str = (String) value;
				if (str.matches("0x0+")) {
					str = "0";
				} else if (str.startsWith("0x")) {
					str = str.replaceFirst("0x0+", "0x");
					str = str.replaceFirst("0x", "");
				}
				setText(str);
			} else if (value instanceof Integer) {
				setText(((Integer) value).toString());
			} else if (value instanceof Boolean) {
				if ((Boolean) value) {
					setIcon(tick);
				} else {
					setIcon(cross);
				}
				setText("");
				setHorizontalTextPosition(CENTER);
			} else if (value instanceof BigInteger) {
				if (table.getColumnName(column).equals("Line No")) {
					setText(((BigInteger) value).toString());
				} else {
					setText(((BigInteger) value).toString(16));
				}
			} else if (value instanceof CellData) {
				CellData data = (CellData) value;
				if (data.qemuValue == null) {
					setText("");
				} else if (data.qemuValue instanceof BigInteger) {
					if (data.quantrValue == null) {
						setText(((BigInteger) data.qemuValue).toString(16));
					} else {
						if (isSame(data.qemuValue, data.quantrValue)) {
							setBackground(isSelected ? matchColorSelected : matchColor);
						} else {
							setBackground(isSelected ? notMatchColorSelected : notMatchColor);
						}

//						setText(((BigInteger) data.qemuValue).toString(16) + " = " + data.quantrValue.toString(16));
						setText(((BigInteger) data.qemuValue).toString(16));
					}
				} else {
					if (data.quantrValue == null) {
						setText(data.qemuValue.toString());
					} else {
//						setText(data.qemuValue + " = " + data.quantrValue);
						setText(data.qemuValue.toString());
					}
				}
			} else {
				setText(value.toString());
			}
		} else {
			setText("NULL");
		}
		setText(" " + getText() + " ");
		return this;
	}

	private boolean isSame(Object o1, Object o2) {
		if (o1 instanceof BigInteger && o2 instanceof BigInteger) {
			BigInteger b1 = (BigInteger) o1;
			BigInteger b2 = (BigInteger) o2;

			if (b1.equals(b2)) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	@Override
	public void validate() {

	}
}
