/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.math.BigInteger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;

import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class DiffTableRenderer extends JLabel implements TableCellRenderer {

	Color selectedBackground = Color.decode("#eee4e1");
	Color lightGray = Color.decode("#f2f2f2");
	Color notMatchColor = Color.decode("#ffc8dd");
	ImageIcon tick = new ImageIcon(getClass().getClassLoader().getResource("image/tick.png"));
	ImageIcon cross = new ImageIcon(getClass().getClassLoader().getResource("image/cross.png"));
	Font defaultFont = getFont();

	public DiffTableRenderer() {
		setOpaque(true);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			setBackground(selectedBackground);
		} else {
			if (row % 2 == 0) {
				setBackground(lightGray);
			} else {
				setBackground(UIManager.getColor("Table.background"));
			}
		}
		setIcon(null);
		setText(null);
		if (value != null) {
			if (column == 0) {
				DiffTableModel model = (DiffTableModel) table.getModel();
				if (model.putOnTopRegisters == null || model.putOnTopRegisters.isEmpty()) {
					setFont(defaultFont);
				} else {
					if (row < model.putOnTopRegisters.size()) {
						setFont(getFont().deriveFont(Font.BOLD));
					} else {
						setFont(defaultFont);
					}
				}
				if (isSame(table.getValueAt(row, 1), table.getValueAt(row, 2))) {
					setIcon(tick);
				} else {
					setIcon(cross);
					setBackground(notMatchColor);
				}
			}
			if (value instanceof BigInteger) {
				if (!isSame(table.getValueAt(row, 1), table.getValueAt(row, 2))) {
					setBackground(notMatchColor);
				}

				String result = ((BigInteger) value).toString(16);
				result = new StringBuilder(result).reverse().toString().replaceAll("(.{4})", "$0 ").trim();
				setText(new StringBuilder(result).reverse().toString());
			} else {
				setText(" " + value.toString());
			}
		}
		return this;
	}

	private boolean isSame(Object o1, Object o2) {
		if (o1 instanceof BigInteger && o2 instanceof BigInteger) {
			BigInteger b1 = (BigInteger) o1;
			BigInteger b2 = (BigInteger) o2;

			if (b1.equals(b2)) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}
