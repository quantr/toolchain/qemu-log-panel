/*
 * Copyright 2024 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.qemulogpanel;

import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Register {

	public final static ArrayList<String> registers = new ArrayList();

	static {
		registers.add("pc");

		//General Purpose Registers
		for (int x = 0; x <= 31; x++) {
			registers.add("x" + x);
		}
		registers.add("zero");
		registers.add("ra");
		registers.add("sp");
		registers.add("gp");
		registers.add("tp");
		registers.add("t0");
		registers.add("t1");
		registers.add("t2");
		registers.add("s0");
		registers.add("s1");
		registers.add("a0");
		registers.add("a1");
		registers.add("a2");
		registers.add("a3");
		registers.add("a4");
		registers.add("a5");
		registers.add("a6");
		registers.add("a7");
		registers.add("s2");
		registers.add("s3");
		registers.add("s4");
		registers.add("s5");
		registers.add("s6");
		registers.add("s7");
		registers.add("s8");
		registers.add("s9");
		registers.add("s10");
		registers.add("s11");
		registers.add("t3");
		registers.add("t4");
		registers.add("t5");
		registers.add("t6");

		//Floating Point Registers
		for (int x = 0; x <= 31; x++) {
			registers.add("f" + x);
		}
		for (int x = 0; x <= 7; x++) {
			registers.add("ft" + x);
		}
		for (int x = 0; x <= 1; x++) {
			registers.add("fs" + x);
		}
		for (int x = 0; x <= 7; x++) {
			registers.add("fa" + x);
		}
		for (int x = 2; x <= 11; x++) {
			registers.add("fs" + x);
		}
		for (int x = 8; x <= 11; x++) {
			registers.add("ft" + x);
		}

		//CSR Registers. doc from https://five-embeddev.com/riscv-isa-manual/latest/priv-csrs.html#csrrwpriv
		registers.add("ustatus");
		registers.add("uie");
		registers.add("utvec");
		registers.add("uscratch");
		registers.add("uepc");
		registers.add("ucause");
		registers.add("utval");
		registers.add("uip");
		registers.add("fflags");
		registers.add("frm");
		registers.add("fcsr");
		registers.add("cycle");
		registers.add("time");
		registers.add("instret");
		for (int x = 3; x <= 31; x++) {
			registers.add("hpmcounter" + x);
		}
		registers.add("cycleh");
		registers.add("timeh");
		registers.add("instreth");
		for (int x = 3; x <= 31; x++) {
			registers.add("hpmcounter" + x + "h");
		}
//		registers.add("sstatus");
		registers.add("sedeleg");
		registers.add("sideleg");
//		registers.add("sie");
		registers.add("stvec");
		registers.add("scounteren");
		registers.add("sscratch");
		registers.add("sepc");
		registers.add("scause");
		registers.add("stval");
		registers.add("sip");
		registers.add("satp");
		registers.add("scontext");
		registers.add("hstatus");
		registers.add("hedeleg");
		registers.add("hideleg");
		registers.add("hie");
		registers.add("hcounteren");
		registers.add("hgeie");
		registers.add("htval");
		registers.add("hip");
		registers.add("hvip");
		registers.add("htinst");
		registers.add("hgeip");
		registers.add("hgatp");
		registers.add("hcontext");
		registers.add("htimedelta");
		registers.add("htimedeltah");
		registers.add("vsstatus");
		registers.add("vsie");
		registers.add("vstvec");
		registers.add("vsscratch");
		registers.add("vsepc");
		registers.add("vscause");
		registers.add("vstval");
		registers.add("vsip");
		registers.add("vsatp");
		registers.add("mvendorid"); //fixed id for hart 
		registers.add("marchid"); // fixed id for hart
		registers.add("mimpid");// fixed value 
		registers.add("mhartid");
		registers.add("mstatus"); // machine mode
		registers.add("misa");
		registers.add("medeleg");
		registers.add("mideleg");
		registers.add("mie");
		registers.add("mtvec");
		registers.add("mcounteren");
		registers.add("mstatush");
		registers.add("mscratch");
		registers.add("mepc");
		registers.add("mcause");
		registers.add("mtval");
		registers.add("mip");
		registers.add("mtinst");
		registers.add("mtval2");
		for (int x = 0; x <= 3; x++) {
			registers.add("pmpcfg" + x);
		}
		for (int x = 0; x <= 15; x++) {
			registers.add("pmpaddr" + x);
		}
		registers.add("mcycle");
		registers.add("minstret");
		for (int x = 3; x <= 31; x++) {
			registers.add("mhpmcounter" + x);
		}
		registers.add("mcycleh");
		registers.add("minstreth");
		for (int x = 3; x <= 31; x++) {
			registers.add("mhpmcounter" + x + "h");
		}
		registers.add("mcountinhibit");
		for (int x = 3; x <= 31; x++) {
			registers.add("mhpmevent" + x);
		}
		registers.add("tselect");
		registers.add("tdata1");
		registers.add("tdata2");
		registers.add("tdata3");
		registers.add("mcontext");
		registers.add("dcsr");
		registers.add("dpc");
		registers.add("dscratch0");
		registers.add("dscratch1");

		//Vector registers
		for (int x = 0; x <= 31; x++) {
			registers.add("v" + x);
		}

		//VCSR
		registers.add("vtype");
		registers.add("vsew");
		registers.add("vlmul");
		registers.add("vta");
		registers.add("vma");
		registers.add("vl");
		registers.add("vlenb");
		registers.add("vstart");
		registers.add("vxrm");
		registers.add("vxsat");
		registers.add("vcsr");

	}

	public static ArrayList<String> getRegisters(String code) {
		if (code.contains("sstatus")) {
			code = code.replaceAll("sstatus", "mstatus");
		}
		ArrayList<String> list = new ArrayList();
		for (String r : registers) {
			if (code.contains(r.toLowerCase())) {
				list.add(r);
			}
		}
		return list;
	}
}
