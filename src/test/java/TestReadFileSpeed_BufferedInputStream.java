
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadFileSpeed_BufferedInputStream {

	@Test
	public void test() throws FileNotFoundException, IOException {
		ProgressBarBuilder pbb = new ProgressBarBuilder().setTaskName("Reading  ").setUnit(" MB", 1048576);
		pbb.showSpeed();
		File qemuLog = new File("../xv6-riscv/qemu.log".replaceFirst("^~", System.getProperty("user.home")));
		BufferedInputStream reader = new BufferedInputStream(ProgressBar.wrap(new FileInputStream(qemuLog), pbb));
		int lineNo = 0;
		String line;
		byte b[] = new byte[102400];
		int x;
		String lastLine = "";
		do {
			x = reader.read(b);
			line = new String(b);
			String lines[] = line.split("\n");
			lines[0] = lastLine + lines[0];
			lastLine = lines[lines.length - 1];
			for (int z = 0; z < lines.length - 1; z++) {
//				System.out.println(">" + lines[z]);
			}
			lineNo++;
		} while (reader.available() > 0);
		System.out.println(lineNo);
	}
}
