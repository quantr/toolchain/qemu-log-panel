
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadFileSpeed_LineIterator {

	@Test
	public void test() throws FileNotFoundException, IOException {
		File qemuLog = new File("../xv6-riscv/qemu.log".replaceFirst("^~", System.getProperty("user.home")));
		LineIterator it = FileUtils.lineIterator(qemuLog, "UTF-8");
		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				// do something with line
			}
		} finally {
			LineIterator.closeQuietly(it);
		}
	}
}
