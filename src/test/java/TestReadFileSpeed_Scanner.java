
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadFileSpeed_Scanner {

	@Test
	public void test() throws FileNotFoundException, IOException {
		ProgressBarBuilder pbb = new ProgressBarBuilder().setTaskName("Reading  ").setUnit(" MB", 1048576);
		pbb.showSpeed();
		File qemuLog = new File("../xv6-riscv/qemu.log".replaceFirst("^~", System.getProperty("user.home")));

		Scanner sc = null;
		sc = new Scanner(ProgressBar.wrap(new FileInputStream(qemuLog), pbb), "UTF-8");
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
		}
	}
}
