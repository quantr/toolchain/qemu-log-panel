
import hk.quantr.qemulogpanel.MainFrame;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/*
 * Copyright 2024 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestClearXMLNodes {

	@Test
	public void test() {
		try {
			DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
			DocumentBuilder b = f.newDocumentBuilder();
			File xml = new File("../riscv-simulator/quantr.xml".replaceFirst("^~", System.getProperty("user.home")));
			Document doc = b.parse(xml);
			XPath xPath = XPathFactory.newInstance().newXPath();
			Node fireInterrupts = (Node) xPath.compile("//fireInterrupts").evaluate(doc, XPathConstants.NODE);
			fireInterrupts.setTextContent(null);
			Node memoryHiJacks = (Node) xPath.compile("//memoryHiJacks").evaluate(doc, XPathConstants.NODE);
			memoryHiJacks.setTextContent(null);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer tf = transformerFactory.newTransformer();
			tf.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource domSource = new DOMSource(doc);
			StreamResult sr = new StreamResult(xml);
			tf.transform(domSource, sr);
			System.out.println("added records to " + xml.getAbsolutePath());
		} catch (IOException | ParserConfigurationException | TransformerException | XPathExpressionException | DOMException | SAXException ex) {
			Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
