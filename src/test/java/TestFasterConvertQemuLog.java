
import static hk.quantr.qemulogpanel.QemuLogPanel.decimalFormatter;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.tongfei.progressbar.ProgressBar;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestFasterConvertQemuLog {

	@Test
	public void test() {
		try {
			File file = new File("../xv6-riscv/qemu.log");
			System.out.println(decimalFormatter.format(file.length()));
			int NO_OF_THREAD = Runtime.getRuntime().availableProcessors() - 2;

			long offset = file.length() / NO_OF_THREAD;
			long offsets[] = new long[NO_OF_THREAD];
			long lengths[] = new long[NO_OF_THREAD];
			for (long x = 0; x < NO_OF_THREAD; x++) {
				System.out.printf("%20s / %s\n", decimalFormatter.format(x * offset), decimalFormatter.format(file.length()));

				if (x > 0) {
					BufferedInputStream fr = new BufferedInputStream(new FileInputStream(file));
					fr.skip(x * offset);
					byte[] c = new byte[2000];
					fr.read(c);
					fr.close();

					String temp = new String(c);
					int tempOffset = temp.indexOf("x28");
					temp = temp.substring(temp.indexOf("x28"));
					tempOffset += temp.indexOf("\n") + 1;
//				System.out.println(tempOffset);

					offsets[(int) x] = (x * offset) + tempOffset;
					if (x == NO_OF_THREAD - 1) {
						lengths[(int) x - 1] = offsets[(int) x] - offsets[(int) x - 1];
						lengths[(int) x] = file.length() - offsets[(int) x];
					} else {
						lengths[(int) x - 1] = offsets[(int) x] - offsets[(int) x - 1];
					}
				}
			}

			ExecutorService executor = Executors.newFixedThreadPool(NO_OF_THREAD);
			final ProgressBar progressBars[] = new ProgressBar[NO_OF_THREAD];

			for (int x = 0; x < NO_OF_THREAD; x++) {
				System.out.println("offsets[x]=" + offsets[x]);
			}
			for (int x = 0; x < NO_OF_THREAD; x++) {
				System.out.println("lengths[x]=" + lengths[x]);
				progressBars[x] = new ProgressBar("thread " + x, lengths[x]);
			}

			for (int x = 0; x < NO_OF_THREAD; x++) {
//				System.out.println(temp);

				final int tempX = x;
				executor.execute(() -> {
					System.out.println("t1=" + tempX);

					try {
						BufferedReader br = new BufferedReader(new FileReader(file));
						System.out.println("t2=" + tempX);

						br.skip(offsets[tempX]);
						System.out.println("t3=" + tempX);
						String line;
						while ((line = br.readLine()) != null) {
							progressBars[tempX].stepBy(line.length());
							if (progressBars[tempX].getCurrent() > lengths[tempX]) {
								break;
							}
						}
						br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				});
			}
			executor.shutdown();
			try {
				executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
			}
		} catch (IOException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}
}
