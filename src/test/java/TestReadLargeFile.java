
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestReadLargeFile {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Test
	public void test() throws IOException {
		System.out.println(sdf.format(new Date()));
		File qemuLog = new File("../xv6-riscv/qemu.log");

		BufferedReader br = Files.newBufferedReader(qemuLog.toPath());
		br.skip(28086222708l);
		System.out.println(br.readLine());

		/*BufferedReader br = Files.newBufferedReader(qemuLog.toPath()); //new BufferedReader(new FileReader(qemuLog));
		String line;
		while ((line = br.readLine()) != null) {

		}
		br.close();*/
 /*Files.lines(qemuLog.toPath()).forEach(line -> {
//			int x = line.length();
		});*/
		System.out.println(sdf.format(new Date()));
	}
}
