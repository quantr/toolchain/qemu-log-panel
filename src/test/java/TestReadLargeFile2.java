
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestReadLargeFile2 {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Test
	public void test() throws IOException {
		System.out.println(sdf.format(new Date()));
		File qemuLog = new File("../xv6-riscv/qemu.log");
		ProgressBarBuilder pbb = new ProgressBarBuilder().setTaskName("Reading  ").setUnit(" MB", 1048576);
		pbb.showSpeed();
		BufferedReader br = new BufferedReader(new InputStreamReader(ProgressBar.wrap(new FileInputStream(qemuLog), pbb)));
		String line;
		ArrayList<String> temp = new ArrayList();
		long lineNo = 0;
		while ((line = br.readLine()) != null) {
			lineNo++;
			temp.add(line);
			if (line.startsWith(" x28")) {
				temp.clear();
			}
		}
		br.close();
		System.out.println(sdf.format(new Date()));
	}
}
