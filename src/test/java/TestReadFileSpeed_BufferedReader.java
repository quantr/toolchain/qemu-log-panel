
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadFileSpeed_BufferedReader {

	@Test
	public void test() throws FileNotFoundException, IOException {
		ProgressBarBuilder pbb = new ProgressBarBuilder().setTaskName("Reading  ").setUnit(" MB", 1048576);
		pbb.showSpeed();
		File qemuLog = new File("../xv6-riscv/qemu.log".replaceFirst("^~", System.getProperty("user.home")));
		BufferedReader reader = new BufferedReader(new InputStreamReader(ProgressBar.wrap(new FileInputStream(qemuLog), pbb)));
		String line;
		int lineNo = 0;
		do {
			line = reader.readLine();
			lineNo++;
		} while (line != null);
		System.out.println(lineNo);
	}
}
