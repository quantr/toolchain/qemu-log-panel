
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ReconstructSequence {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Test
	public void test() throws IOException, SQLException {
		Connection conn = DriverManager.getConnection("jdbc:h2:./database", "sa", "");
		conn.setAutoCommit(false);
		ResultSet rs = conn.prepareStatement("select id, sequence from data order by sequence").executeQuery();
		long sequence = 0;
		while (rs.next()) {
			if (sequence != rs.getLong("sequence")) {
				conn.prepareStatement("update data set sequence=" + sequence + " where id=" + rs.getInt("id"));
			}
			sequence++;
		}

		conn.close();

	}
}
