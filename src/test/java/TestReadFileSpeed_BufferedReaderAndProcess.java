
import hk.quantr.javalib.CommonLib;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadFileSpeed_BufferedReaderAndProcess {

	@Test
	public void test() throws FileNotFoundException, IOException {
		ProgressBarBuilder pbb = new ProgressBarBuilder().setTaskName("Reading  ").setUnit(" MB", 1048576);
		pbb.showSpeed();
		File qemuLog = new File("../xv6-riscv/qemu.log".replaceFirst("^~", System.getProperty("user.home")));
		BufferedReader reader = new BufferedReader(new InputStreamReader(ProgressBar.wrap(new FileInputStream(qemuLog), pbb)));
		String line = null;
		int lineNo = 0;
		long sequence = 0;
		AtomicLong noOfPending = new AtomicLong();
		Pattern p = Pattern.compile("\\[(.*)\\]");
		ArrayList<String> nextRecordLines = new ArrayList(); // scan mepc of next record
		ArrayList<String> tempLines = new ArrayList();
		int priv = -1; // because priv not appear every sector in qemu.log, so can't parse it inside H2Thread, has to be parse it outside to preseve previous value
		outer:
		do {
			ArrayList<String> lines = new ArrayList();

			if (!nextRecordLines.isEmpty()) {
				lines.addAll(nextRecordLines);
				nextRecordLines.clear();
				continue;
			}

			if (!tempLines.isEmpty()) {  // add back lines that read to current lines e.g. memread / memwrite
				lines.addAll(tempLines);
				tempLines.clear();
			}

			do {
				line = reader.readLine();
				lineNo++;
				if (line == null) {
					break outer;
				}
				lines.add(line);
			} while (!line.startsWith(" x28"));

			// see if hv interrupt
			tempLines.clear();
			for (int i = 0; i < 3; i++) {
				line = reader.readLine();
				lineNo++;
				if (line == null) {
					break outer;
				}
				tempLines.add(line);
				if (line.startsWith(" V")) { // reached next record, stop immediately, clear useless temp lines
					break;
				} else if (line.startsWith("riscv_cpu_do_interrupt") && line.contains("desc=m_timer")) { // have interrupt
					// check if the record needs to be ignored
					// if pc of current record == mepc of next record, then skip
					long pc = 0, mepc = 0;

					for (String temp : lines) { // scan pc
						if (temp.startsWith(" pc")) {
							pc = Long.parseLong(temp.replace(" pc       ", ""), 16);
							break;
						}
					}

					do {
						line = reader.readLine();
						lineNo++;
						if (line == null) {
							break outer;
						}
						nextRecordLines.add(line);
						if (line.startsWith(" mepc")) {
							mepc = Long.parseLong(line.replace(" mepc     ", ""), 16);
						}
					} while (!line.startsWith(" x28"));

					// compare
					if (pc == mepc) { // ignore current record
						lines.clear();
						lines.addAll(tempLines);
						lines.addAll(nextRecordLines); // put the next record to lines
						tempLines.clear();
						nextRecordLines.clear();
						break;
					} else {
						lines.addAll(tempLines); // add back the interrupt line to lines
						tempLines.clear();
						break;
					}
				}
			}
			// end checking interrupt

			// check skip
			long skipAddress = 0;
			boolean skip = false;
			boolean skipCPU_recompile = true;
			long pc = -1;

			for (String temp : lines) {
				if (temp.startsWith("Stopped execution of TB chain")) {
					skip = true;
					Matcher m = p.matcher(temp);
					m.find();
					skipAddress = CommonLib.string2long("0x" + m.group(1));
				} else if (temp.startsWith(" pc")) {
					String words[] = temp.trim().split(" +");
					String value = words[1].startsWith("0x") ? words[1] : "0x" + words[1];
					pc = CommonLib.string2long(value);
					break;
				} else if (temp.startsWith("cpu_io_recompile")) {
					skipCPU_recompile = false;
				} else if (temp.startsWith("Priv")) {
					priv = Integer.parseInt(temp.split(";")[0].split(": ")[1]);
				}
			}
			// end check skip

			if (!(skip && pc == skipAddress) && skipCPU_recompile) {
				noOfPending.addAndGet(1);
//				MyThread thread = new MyThread(pc, sequence, lines, progressBars, lineNo, allCode, priv, stmt, noOfPending);
//				executor.submit(thread);
				sequence++;

//				if (sequence % 100000 == 0) {
//					conn.commit();
//				}
//				while (noOfPending.get() > 100000) {
//					try {
//						Thread.sleep(2000);
//					} catch (InterruptedException ex) {
//						Logger.getLogger(QemuLogPanel.class.getName()).log(Level.SEVERE, null, ex);
//					}
//				}
			}
		} while (line != null);
		System.out.println(lineNo);
	}
}
