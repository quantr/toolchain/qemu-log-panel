
import hk.quantr.javalib.CommonLib;
import java.math.BigInteger;
import org.junit.Test;

/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

	@Test
	public void test1() {
		BigInteger b = CommonLib.string2BigInteger("0xffffffffffffe000");
		System.out.println(b);
		System.out.println(Long.toHexString(b.longValue()));
	}
}
