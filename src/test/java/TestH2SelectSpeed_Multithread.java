
import hk.quantr.qemulogpanel.QemuLogPanel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.tongfei.progressbar.ProgressBar;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestH2SelectSpeed_Multithread {

	@Test
	public void test() throws FileNotFoundException, IOException, SQLException {
		String path = new File("../xv6-riscv/database.mv.db").getAbsolutePath().replace(".mv.db", "");
		System.out.println(path);
		Connection conn = DriverManager.getConnection("jdbc:h2:" + path + ";CACHE_SIZE=13107200;PAGE_SIZE=10240;CACHE_TYPE=SOFT_LRU;", "sa", "");

//		int pageNo = 10;
//		int pageSize = 10000;
//		String sql = "select * from `quantr` where sequence>=" + (pageNo * pageSize) + " and sequence<" + ((pageNo + 1) * pageSize) + " order by sequence limit " + pageSize;
//		System.out.println(sql);
//		try {
//			ResultSet qemuRS = qemuStmt.executeQuery(sql);
//			qemuRS.next();
//			System.out.println(qemuRS.getLong("sequence"));
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//		}
		int pageSize = 10000;
		int NO_OF_THREAD = 4;//Runtime.getRuntime().availableProcessors() - 1;
		ExecutorService executor = Executors.newFixedThreadPool(NO_OF_THREAD);
		ProgressBar pb = new ProgressBar("Thread", 1000);
		for (int q = 0; q < NO_OF_THREAD; q++) {
			final int temp = q;
			executor.submit(new Thread() {
				@Override
				public void run() {
					final int pageNo = temp;
					String sql = "select * from `quantr` where sequence>=" + (pageNo * pageSize) + " and sequence<" + ((pageNo + 1) * pageSize) + " order by sequence limit " + pageSize;
					System.out.println(sql);
					try {
						Statement qemuStmt = conn.createStatement();
						ResultSet qemuRS = qemuStmt.executeQuery(sql);
						qemuRS.next();
						System.out.println(qemuRS.getLong("sequence"));
					} catch (SQLException ex) {
						ex.printStackTrace();
					}
				}
			}
			);
		}
		executor.shutdown();
		try {
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException ex) {
			Logger.getLogger(QemuLogPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
		pb.refresh();
		conn.commit();
		conn.close();
	}
}
