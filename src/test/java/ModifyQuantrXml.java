
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ModifyQuantrXml {

	@Test
	public void test() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerException {
		String line = "riscv_cpu_do_interrupt: hart:0, async:1, cause:0000000000000009, epc:0x0000000080000c7e, tval:0x0000000000000000, desc=s_external";
		if (line.equals("") || line.startsWith("IN") || line.startsWith("Priv") || line.contains("----") || line.contains("V      =   0") || line.contains("Trace")) {
			System.out.println("ok");
		} else if (line.startsWith("riscv_cpu_do_interrupt")) {
			System.out.println("ok2");
		}

//		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
//		DocumentBuilder b = f.newDocumentBuilder();
//		Document doc = b.parse(new File("../riscv-simulator/quantr.xml"));
//		XPath xPath = XPathFactory.newInstance().newXPath();
//		Node fireInterrupts = (Node) xPath.compile("//fireInterrupts").evaluate(doc, XPathConstants.NODE);
////		System.out.println(fireInterrupts.getTextContent());
////		fireInterrupts.setTextContent("fuck");
//		fireInterrupts.setTextContent(null);
//
//		Transformer tf = TransformerFactory.newInstance().newTransformer();
////		tf.setOutputProperty(OutputKeys.INDENT, "yes");
////		tf.setOutputProperty(OutputKeys.METHOD, "xml");
////		tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
//
//		DOMSource domSource = new DOMSource(doc);
//		StreamResult sr = new StreamResult(new File("../riscv-simulator/quantr.xml"));
//		tf.transform(domSource, sr);
	}
}
