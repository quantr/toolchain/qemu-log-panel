
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadFileSpeed_SeekableByteChannel {

	@Test
	public void test() throws FileNotFoundException, IOException {
		File qemuLog = new File("../xv6-riscv/qemu.log".replaceFirst("^~", System.getProperty("user.home")));
		try (SeekableByteChannel ch = java.nio.file.Files.newByteChannel(Path.of(qemuLog.getAbsolutePath()), StandardOpenOption.READ)) {
			ByteBuffer bf = ByteBuffer.allocate(1000);
			while (ch.read(bf) > 0) {
				bf.flip();
				// System.out.println(new String(bf.array()));
				bf.clear();
			}
		}
	}
}
