
import hk.quantr.javalib.CommonLib;
import static hk.quantr.qemulogpanel.QemuLogPanel.initKernelCodeCache;
import hk.quantr.qemulogpanel.SourceData;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JProgressBar;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestLogToFile {

	static Pattern p = Pattern.compile("\\[(.*)\\]");
	static Pattern r = Pattern.compile(".*addr (0x[0-9a-fA-F]+).* value (0x[0-9a-fA-F]+).* size ([0-9]+)");

	@Test
	public void test() throws SQLException, IOException {
		File qemuLog = new File("~/workspace/xv6-riscv/qemu.log".replaceFirst("^~", System.getProperty("user.home")));

		JProgressBar progressBar = null;
		ConcurrentHashMap<Long, SourceData> allCode = new ConcurrentHashMap();
		initKernelCodeCache(allCode, progressBar);

		ProgressBarBuilder pbb = new ProgressBarBuilder().setTaskName("Reading  ").setUnit(" MB", 1048576);
		pbb.showSpeed();

//			BufferedReader reader = new BufferedReader(new FileReader(qemuLog), 1000000);
		BufferedReader reader = new BufferedReader(new InputStreamReader(ProgressBar.wrap(new FileInputStream(qemuLog), pbb)));
		BufferedWriter writer = new BufferedWriter(new FileWriter("fuck.txt"));

		final int NO_OF_THREAD = 1;//Runtime.getRuntime().availableProcessors() - 1;

		ProgressBar progressBars[] = new ProgressBar[NO_OF_THREAD];
		for (int x = 0; x < NO_OF_THREAD; x++) {
			progressBars[x] = new ProgressBar(String.format("Thread %02d", x), qemuLog.length());
		}
		String lineRaw;
		long lineNo = 0;

		AtomicLong noOfPending = new AtomicLong();
//			HashMap<Long, ArrayList<String>> interrupts = new HashMap();
//			boolean skipInterruptLog = false;
		boolean interruptLogged = false;
		String interruptLine = null;
		ArrayList<String> tempLines = new ArrayList();
		outer:
		do {
			ArrayList<String> lines = new ArrayList();
			if (!tempLines.isEmpty()) {
				lines.addAll(tempLines);
				tempLines.clear();
			}
			if (interruptLogged && interruptLine != null) {
//					System.out.println("??");interruptLogged
				lines.add(interruptLine);
				interruptLogged = false;
			}
			do {
				lineRaw = reader.readLine();
				progressBars[0].stepBy(lineRaw.length());
//				System.out.println(line);
				if (lineRaw == null) {
					break outer;
				}
				lineNo++;
				lines.add(lineRaw);
			} while (!lineRaw.startsWith(" x28"));

			for (int i = 0; i < 3; i++) { // see if hv interrupt
				lineRaw = reader.readLine();
				if (lineRaw == null) {
					break outer;
				}
				lineNo++;
				if (lineRaw.startsWith("riscv_cpu_do_interrupt") && lineRaw.contains("desc=m_timer")) {
					interruptLogged = true;
					interruptLine = lineRaw;
					break;
				} else {
					tempLines.add(lineRaw);
				}
			}

			// check skip
			long skipAddress = 0;
			boolean skip = false;
			boolean skipCPU_recompile = true;
			long pc = -1;

			for (String temp : lines) {
				if (temp.startsWith("Stopped execution of TB chain")) {
					skip = true;
					Matcher m = p.matcher(temp);
					m.find();
					skipAddress = CommonLib.string2long("0x" + m.group(1));
				} else if (temp.startsWith(" pc")) {
					String words[] = temp.trim().split(" +");
					String value = words[1].startsWith("0x") ? words[1] : "0x" + words[1];
					pc = CommonLib.string2long(value);
					break;
				} else if (temp.startsWith("cpu_io_recompile")) {
					skipCPU_recompile = false;
				}
			}
			// end check skip

			if (!(skip && pc == skipAddress) && skipCPU_recompile && !interruptLogged) {
				noOfPending.addAndGet(1);

				String code = null;
				boolean mem = false;
				String memOperation = null;
				long memAddr = -1;
				long memValue = -1;
				short memSize = -1;
				boolean memRead = false;
				boolean interrupt = false;
				long cause = -1;
				String desc = null;
				boolean irqRequest = false;
				int irqRequestNo = -1;
				int irqRequestLevel = -1;

				for (String line : lines) {
//				dump += line + "\n";
					if (line.equals("") || line.startsWith("IN") || line.startsWith("Priv") || line.contains("----") || line.contains("V      =   0") || line.contains("Trace") || line.contains("riscv_cpu_tlb_fill") || line.contains("cpu_io_recompile")) {
					} else if (line.startsWith("sifive_plic_irq_request")) {
						irqRequest = true;
						irqRequestNo = Integer.parseInt(line.split(",")[1].split("=")[1]);
						irqRequestLevel = Integer.parseInt(line.split(",")[2].split("=")[1]);
					} else if (line.startsWith("Stopped execution of TB chain")) {
//					skip = true;
//					Matcher m = p.matcher(line);
//					m.find();
//					skipAddress = CommonLib.string2long("0x" + m.group(1));
					} else if (line.startsWith("riscv_cpu_do_interrupt")) {
						interrupt = true;
//					System.out.println(line.replaceFirst("^.*?:", ""));
						String temp[] = line.replaceFirst("^.*?:", "").split(",");
//					System.out.println(temp.length);
						cause = CommonLib.string2long(temp[2].split(":")[1].trim());
						desc = temp[5].split("=")[1].trim();
//					System.out.println("interrupt");
					} else if (line.startsWith("0x")) {
						code = line;
//				} else if (line.startsWith("riscv_cpu_tlb_fill")) {
//					if (line.startsWith("riscv_cpu_tlb_fill ad") && (line.contains("rw 0") || line.contains("rw 1"))) {
//						mem = line.replaceFirst("riscv_cpu_tlb_fill ad", "");
//						mem = mem.replaceAll("mmu_idx.*", "");
//						mem = mem.trim();
//					}
					} else if (line.startsWith("memory_region")) {
						mem = true;
						memOperation = line;
						Matcher m = r.matcher(line);
						m.find();
						memAddr = CommonLib.string2long(m.group(1));
						memValue = CommonLib.string2BigInteger(m.group(2)).longValue();
						memSize = CommonLib.string2short(m.group(3));
						if (line.contains("read")) {
							memRead = true;
						}
					} else {
						String words[] = line.trim().split(" +");
						for (int x = 0; x < words.length - 1; x += 2) {
							words[x] = words[x].replaceAll("/", "_");
//						if (x + 1 > words.length - 1) {
//							System.out.println("fuck : " + line);
//							System.exit(1);
//						}
							String value = words[x + 1].startsWith("0x") ? words[x + 1] : "0x" + words[x + 1];
							if (!words[x].contains(":")) {
//								values.put(words[x], value);
								writer.write(words[x] + "=" + value + ",");
							}
//						if (words[x].trim().equals("pc")) {
//							pc = CommonLib.string2long(value);
//						}
						}
					}
				}
				writer.write('\n');

			}

		} while (lineRaw != null);
		reader.close();
		writer.close();
	}
}
