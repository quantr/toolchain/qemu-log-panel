# QEMU Log Panel

Software to visualize the qemu log and provide diff to the dump.json from our simulator. We use this to debug our simulator, the correctness of each insturction exection.

![](https://www.quantr.foundation/wp-content/uploads/2022/10/qemu-log-panel20221012.png)

# Modify QEMU

Log all instructions

https://peter.quantr.hk/2023/12/risc-v-qemu-doesnt-log-priv-in-every-instruction

Log all memory operations

https://peter.quantr.hk/2024/01/qemu-risc-v-log-all-memory-operations/

https://peter.quantr.hk/2024/02/the-way-to-extend-qemu-tcg-plugin-functionality/

# Usage

```
java -jar target/qemu-log-panel-1.0.jar -g
```

Command line convert qemu log to h2
```
java -jar target/qemu-log-panel-1.0.jar  -f qemu.log -t
```

## Create qemu.log

Before qemu 9.0
```
qemu2: $K/kernel fs.img
    $(QEMU) $(QEMUOPTS) -singlestep -d exec,cpu,nochain,in_asm,int,trace:memory_region_ops_read,trace:memory_region_ops_write -D qemu.log
```

After qemu 9.0

Linux

```
qemu2: $K/kernel fs.img
    $(QEMU) $(QEMUOPTS) -accel tcg,one-insn-per-tb=on -d exec,cpu,nochain,in_asm,int,trace:memory_region_ops_read,trace:memory_region_ops_write,plugin -plugin ~/workspace/qemu/build/tests/plugin/libmem.so,callback=true -D qemu.log
```

Mac

```
qemu2: $K/kernel fs.img
    $(QEMU) $(QEMUOPTS) -accel tcg,one-insn-per-tb=on -d exec,cpu,nochain,in_asm,int,trace:memory_region_ops_read,trace:memory_region_ops_write,plugin -plugin ~/workspace/qemu/build/tests/plugin/libmem.dylib,callback=true -D qemu.log
```

## Launch gui and load qemu log

```
java -jar target/qemu-log-panel-1.0.jar -g -f ../xv6-riscv/qemu.log
```

Connect to H2 database and show the gui
```
java -jar target/qemu-log-panel-1.0.jar -g -d ../xv6-riscv/database.mv.db --h2 ../riscv-simulator/dump.mv.db
```

Generate quantr.xml
```
quantrxml:
	java -jar ../qemu-log-panel/target/qemu-log-panel-1.0-jar-with-dependencies.jar -e ../riscv-simulator/quantr.xml
```

Generate h2 database
```
h2:
	java -jar ../qemu-log-panel/target/qemu-log-panel-1.0-jar-with-dependencies.jar -f qemu.log -t

info:
	java -jar ../qemu-log-panel/target/qemu-log-panel-1.0-jar-with-dependencies.jar -f qemu.log -i
```

# Explain

This program will read the qemu.log into H2 database, to open the H2 database by squirrel sql, follow these steps:

1. Download https://squirrel-sql.sourceforge.io/
1. Check "H2" during installation
1. Run Squirrel sql, if in mac it probably find wrong jdk, then edit "/Applications/SQuirreLSQL.app/Contents/MacOS/squirrel-sql.sh", add "JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk-18.0.1.1.jdk/Contents/Home", to find out what jdk in your mac, type "/usr/libexec/java_home -V". Warning, you have to pick jdk home from "/usr/libexec/java_home -V", other than this will not work
1. Download H2 from https://www.h2database.com/html/main.html
1. Add the jar to Squirrel sql

# Use this tool with XV6 and our simulator

1. Use our xv6-riscv fork https://github.com/quantrpeter/xv6-riscv , make sure clone it in the same parent folder of riscv-simulator project
1. make qemu , stop it by ctrl-a x, then you have qemu.log, make sure this file don't be too large
1. make h2, it will convert qemu.log into database.db (H2 db file)
1. Run the riscv-simulator project to simulate the XV6, then you have dump.json
1. Now you have both H2 db file and dump.json, we can do the comparison now
1. Run this project in netbeans, you can see the parameters, it pointed to database.mv.db and dump.json
![](https://www.quantr.foundation/wp-content/uploads/2022/10/run-qemu-log-panel.png)
1. Interface is popup as below
![](https://www.quantr.foundation/wp-content/uploads/2022/10/qemu-log-panel-usage.png)

# Run in netbeans

run ui:

-g -d ../xv6-riscv/database.mv.db --h2 ../riscv-simulator/dump.mv.db

run converting qemu.log to h2

-f qemu.log

# wc/lc

Qemu-log-panel is using "wc -l" and "lc" to count how many line in qemu. "lc" is much faster, so install https://github.com/p-ranav/lc

# Performance

## script

```
#!/bin/bash
curl https://www.quantr.foundation/jdk-21_macos-aarch64_bin.tar --output jdk-21_macos-aarch64_bin.tar
tar xvf jdk-21_macos-aarch64_bin.tar
curl https://www.quantr.foundation/apache-maven-3.9.6-bin.tar.gz --output apache-maven-3.9.6-bin.tar.gz
tar zxvf apache-maven-3.9.6-bin.tar.gz
export JAVA_HOME=~/jdk-21.0.1.jdk/Contents/Home
export PATH=$JAVA_HOME/bin:~/apache-maven-3.9.6/bin:$PATH
mkdir workspace
cd workspace
scp -r -P 2207 peter@quantr.hk:~/2TB/workspace/qemu-log-panel .
scp -r -P 2207 peter@quantr.hk:~/2TB/workspace/xv6-riscv .
cd qemu-log-panel
```

## Tests

```
mvn -Dtest=TestReadFileSpeed_BufferedReader --no-transfer-progress process-test-classes surefire:test
```

```
mvn -Dtest=TestReadFileSpeed_BufferedReaderAndProcess --no-transfer-progress process-test-classes surefire:test
```

```
mvn -Dtest=TestH2InsertSpeed --no-transfer-progress process-test-classes surefire:test
```

```
mvn -Dtest=TestH2InsertSpeed_Tcp --no-transfer-progress process-test-classes surefire:test
```

```
mvn -Dtest=TestH2InsertSpeed_Multithread --no-transfer-progress process-test-classes surefire:test
```

# Notes

# Netbeans run profile

Argument: -g -d ../xv6-riscv/database.mv.db --h2 ../riscv-simulator/dump.mv.db

Working Direcotry: <empty>

